public with sharing class VGUtils 
{
    //Convert a SQL string into SOQL,features will be added 
    public static String SQLParse(String q)
    {
        if(q != null && q.contains(' * '))
        {
            String[] aQuery = q.split('FROM');
            String sObjName = aQuery[1].trim().split(' ')[0].trim(); 

            Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjName).getDescribe().fields.getMap();
            String sFieldsList = '';
            Integer iCounter = 0;
            for(Schema.SObjectField sfield : fieldMap.Values())
            {
                iCounter++;

                schema.describefieldresult dfield = sfield.getDescribe();
                sFieldsList += dfield.getname();
                if(iCounter < fieldMap.Values().size())
                {
                    sFieldsList += ', ';
                }
            }

            q = q.replace('*', sFieldsList);
        }
        return q;
    }

    public static sObject[] SQLRun(String q)
    {
        return database.query(SQLParse(q));
    }

    //Methods to quick send emails
    public static void sendEmail(String address, String subject, String body) 
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {address};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setHtmlBody(body);
        Messaging.SendEmailResult[] results = Messaging.sendEmail(
                                    new Messaging.SingleEmailMessage[] { mail });
    }

    public static void logMail(String adress,String Body)
    {
        datetime dt = datetime.now();
        string sDt = dt.format();
        sendEmail(adress,'SFDC LOG | ' + sDt ,body);
    }

    public static Object IIF(Boolean condition, Object retTrue, Object retFalse )
    {
        if(condition)
            return retTrue;
        else
            return retFalse;
    }

    public static String    IIF(Boolean condition, String retTrue, String retFalse ){return (String)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Integer   IIF(Boolean condition, Integer retTrue, Integer retFalse ){return (Integer)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Decimal   IIF(Boolean condition, Decimal retTrue, Decimal retFalse ){return (Decimal)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Double    IIF(Boolean condition, Double retTrue, Double retFalse ){return (Double)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static ID        IIF(Boolean condition, ID retTrue, ID retFalse ){return (ID)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Blob      IIF(Boolean condition, Blob retTrue, Blob retFalse ){return (Blob)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Boolean   IIF(Boolean condition, Boolean retTrue, Boolean retFalse ){return (Boolean)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Date      IIF(Boolean condition, Date retTrue, Date retFalse ){return (Date)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static DateTime  IIF(Boolean condition, DateTime retTrue, DateTime retFalse ){return (DateTime)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Long      IIF(Boolean condition, Long retTrue, Long retFalse ){return (Long)IIF(condition, (Object)retTrue, (Object)retFalse);}
    public static Time      IIF(Boolean condition, Time retTrue, Time retFalse ){return (Time)IIF(condition, (Object)retTrue, (Object)retFalse);}

    public static boolean ISNULL(Object o)
    {
        return (o == null);
    }
    
    public static Object IFNULL(Object check, Object ret)
    {
        if(ISNULL(check))
            return ret;
        else
            return check;
    }

    public static String    IFNULL(String check, String ret){return (String)IFNULL((Object)check, (Object)ret);}
    public static Integer   IFNULL(Integer check, Integer ret){return (Integer)IFNULL((Object)check, (Object)ret);}
    public static Decimal   IFNULL(Decimal check, Decimal ret){return (Decimal)IFNULL((Object)check, (Object)ret);}
    public static Double    IFNULL(Double check, Double ret){return (Double)IFNULL((Object)check, (Object)ret);}
    public static ID        IFNULL(ID check, ID ret){return (ID)IFNULL((Object)check, (Object)ret);}
    public static Blob      IFNULL(Blob check, Blob ret){return (Blob)IFNULL((Object)check, (Object)ret);}
    public static Boolean   IFNULL(Boolean check, Boolean ret){return (Boolean)IFNULL((Object)check, (Object)ret);}
    public static Date      IFNULL(Date check, Date ret){return (Date)IFNULL((Object)check, (Object)ret);}
    public static DateTime  IFNULL(DateTime check, DateTime ret){return (DateTime)IFNULL((Object)check, (Object)ret);}
    public static Long      IFNULL(Long check, Long ret){return (Long)IFNULL((Object)check, (Object)ret);}
    public static Time      IFNULL(Time check, Time ret){return (Time)IFNULL((Object)check, (Object)ret);}

    public static String IFEMPTY(String check, String ret)
    {
        if(String.isEmpty(check))
            return ret; 
        else
            return check;
    }
}