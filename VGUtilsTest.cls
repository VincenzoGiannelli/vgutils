@isTest
public class VGUtilsTest 
{
    @isTest
    static void SQLRunTest()
    {
        Account a = new Account();
        a.Name = 'Test01';
        insert a;

        List<Account> la = (List<Account>)VGUtils.SQLRun('SELECT * FROM Account');

        System.assertEquals(la.size(),1);
        System.assertEquals(la[0].Name,'Test01');

    }

    @isTest
    static void logMailTest()
    {
        VGUtils.logMail('test@test.com','');
    }

    @isTest
    static void IIFTest()
    {
        Boolean b = true;

        System.assertEquals('test', VGUtils.IIF(b,'test','test2'));
        System.assertEquals(0, VGUtils.IIF(!b,1,0));
        Long l1 = 0;
        Long l2 = 100;
        System.assertEquals(l2, VGUtils.IIF(!b,l1,l2));
        System.assertEquals((Decimal)l2, VGUtils.IIF(!b,(Decimal)l1,(decimal)l2));
        System.assertEquals((Double)l2, VGUtils.IIF(!b,(Double)l1,(Double)l2));
        System.assertEquals(true, VGUtils.IIF(b,true,false));
        System.assertEquals(Date.newinstance(1960, 2, 17), VGUtils.IIF(b,date.newinstance(1960, 2, 17),date.newinstance(1990, 6, 11)));
        System.assertEquals(DateTime.newInstance(1341828183000L), VGUtils.IIF(b,DateTime.newInstance(1341828183000L),DateTime.newInstance(1111828182000L)));
        System.assertEquals(Blob.valueOf('HELLOWORLD'), VGUtils.IIF(b,Blob.valueOf('HELLOWORLD'),Blob.valueOf('HHH')));
        System.assertEquals(Time.newInstance(18, 30, 2, 20),VGUtils.IIF(b,Time.newInstance(18, 30, 2, 20),Time.newInstance(19, 30, 3, 25)));

        Account acc1 = new Account();
        acc1.Name = 'test';
        insert acc1;

        Account acc2 = new Account();
        acc2.Name = 'test2';
        insert acc2;
        System.assertEquals(acc1.Id, VGUtils.IIF(b,acc1.Id,acc2.Id));
    }

    @isTest
    static void IFEmptyTest()
    {
        System.assertEquals('TEST',VGUtils.IFEmpty('','TEST'));
        System.assertEquals('TEST',VGUtils.IFEmpty('TEST','TEST2'));
    }

    @isTest
    static void IFNullTest()
    {
        System.assertEquals('TEST',VGUtils.IFNull((string)null,'TEST'));
        System.assertEquals('TEST',VGUtils.IFNull('TEST','TEST2'));
        System.assertEquals(1,VGUtils.IFNull((Integer)null,1));
        System.assertEquals((Long)1,VGUtils.IFNull((Long)null,1));
        System.assertEquals((Decimal)1,VGUtils.IFNull((Decimal)null,1));
        System.assertEquals((Double)1,VGUtils.IFNull((Double)null,1));
        System.assertEquals(true,VGUtils.IFNull((Boolean)null,true));
        System.assertEquals(Date.newinstance(1960, 2, 17), VGUtils.IFNull((Date)null,date.newinstance(1960, 2, 17)));
        System.assertEquals(DateTime.newInstance(1341828183000L), VGUtils.IFNull((DateTime)null,DateTime.newInstance(1341828183000L)));
        System.assertEquals(Time.newInstance(18, 30, 2, 20),VGUtils.IFNULL((Time)null,Time.newInstance(18, 30, 2, 20)));
        System.assertEquals(Blob.valueOf('HELLOWORLD'),VGUtils.IFNULL((Blob)null,Blob.valueOf('HELLOWORLD')));

        Account a = new Account();
        a.Name = 'test account';
        insert a;

        System.assertEquals(a.Id,VGUtils.IFNULL((Id)null,a.Id));
    }
}