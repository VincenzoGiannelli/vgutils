# VGUtils.cls

A simple utility class intended to be used in Salesforce.com projects; functionalities may be added not regularly.

You can use the code as reference or use the classes in your projects.

For questions or suggestions write to _inbox.giannelli@gmail.com_.

## SQLRun(query)
Converts a SQL query into a SOQL and executes it, returns a list of SObjects. Currently only supports '*' character.
#### Usage:
```
List<Account> a = (List<Account>)VGUtils.SQLRun('SELECT * FROM Account');
```

## SQLParse(query)
Converts a SQL query into a SOQL , does not executes the query, returns the query in String format. Currently only supports '*' character.
#### Usage:
```
String s = VGUtils.SQLParse('SELECT * FROM Account');
```

## logMail(adress, body)
Send a quick debug message via e-mail, the message subject will have the following format: "SFDC LOG | DD/MM/YYYY hh.mm", e-mail address and content can be specified. Supports html in email body.
#### Usage:
```
VGUtils.logMail('test@test.com','this is the ccntent of my email');
```

## sendEmail(address, subject, body)
A simple method used to send email message. Supports html in email body.
#### Usage:
```
VGUtils.sendEmail('test@test.com','Example Mail','<div style="color:red;">Hi, this is an <b>example</b> mail!</div>');
```

## IIF(Boolean condition, Object retTrue, Object retFalse )
Inline conditional. Return first object if true, second if false.
#### Usage:
```
Integer i = 5;
String status = VGUtils.IIF(i<=10,'OK', 'SOMETHING WRONG');
```

## ISNULL(Object o)
Check if parameter is null, return a boolean value.
#### Usage:
```
Boolean b = VGUtils.ISNULL('TEST');
```

## IFNULL(Object check, Object ret)
Check if first parameter is null, if so, return the second parameter, if not, return the first.
#### Usage:
```
Integer res = getStatus(); 
Integer status = VGUtils.IFNULL(res, -1);
```

## IFEMPTY(String check, String ret)
Check if first string is empty, if so, return the second string, if not, return the first.
#### Usage:
```
String res = getStatus(); 
String status = VGUtils.IFEMPTY(res, 'KO');
```